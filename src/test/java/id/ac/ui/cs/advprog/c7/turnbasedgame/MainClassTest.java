package id.ac.ui.cs.advprog.c7.turnbasedgame.frontend;

import id.ac.ui.cs.advprog.c7.turnbasedgame.TurnBasedGameApplication;
import org.junit.jupiter.api.Test;

public class MainClassTest {
    @Test
    public void main() {
        TurnBasedGameApplication.main(new String[] {});
    }
}

