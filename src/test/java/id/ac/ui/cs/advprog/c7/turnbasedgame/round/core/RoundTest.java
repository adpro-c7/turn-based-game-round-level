package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class RoundTest {

    private Class<?> roundClass;

    @InjectMocks
    private Round round;

    @BeforeEach
    public void setUp() throws Exception {
        roundClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round");
        round = new Round();
        round.addRound();
        round.setNewMaxRound();
    }

    @Test
    public void testRoundIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(roundClass.getModifiers()));
    }

    @Test
    public void testAddRoundMethodReturnCorrectAnswer() throws Exception{
        round.addRound();
        assertEquals(3, round.getCurrentRound());
    }

    @Test
    public void testResetRoundMethodReturnCorrectAnswer() throws Exception{
        round.resetRound();
        assertEquals(0, round.getCurrentRound());
    }

    @Test
    public void testGetCurrentRoundReturnCorrectAnswer() throws Exception{
        assertEquals(2, round.getCurrentRound());
    }

    @Test
    public void testGetMaxRoundReturnCorrectAnswer() throws Exception{
        round.addRound();
        round.setNewMaxRound();
        assertEquals(2, round.getMaxRound());
    }

    @Test
    public void testSetNewMaxRoundMethodReturnCorrectAnswer() throws Exception{
        for (int i=0; i<10; i++){
            round.addRound();
        }
        round.setNewMaxRound();
        assertEquals(11, round.getMaxRound());

        round.resetRound();
        round.addRound();
        round.setNewMaxRound();
        assertEquals(11, round.getMaxRound());
    }

    // TODO: buat test untuk update
    @Test
    public void testUpdateReturnCorrectAnswer() throws Exception{
        round.update("WIN");
        assertEquals(2, round.getMaxRound());
        assertEquals(3, round.getCurrentRound());

        round.update("LOSE");
        assertEquals(2, round.getMaxRound());
        assertEquals(0, round.getCurrentRound());

    }

}
