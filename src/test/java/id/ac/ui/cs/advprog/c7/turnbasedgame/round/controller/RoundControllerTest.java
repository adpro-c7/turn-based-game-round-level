package id.ac.ui.cs.advprog.c7.turnbasedgame.round.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = RoundController.class)
public class RoundControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private RoundService roundService;

    private Round round;

    @BeforeEach
    public void setUp(){
        round = new Round();
        roundService.save("tokenTest", round);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerFindByTokenRound() throws Exception{
        when(roundService.findByToken("tokenTest")).thenReturn(round);
        mvc.perform(get("/v1/round/findRoundBy/tokenTest").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentRound").value(1))
                .andExpect(jsonPath("$.maxRound").value(0));
        when(roundService.findByToken("tokenTest")).thenReturn(null);
        mvc.perform(get("/v1/round/findRoundBy/tokenTestBelumAda").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerCreateRound() throws Exception {
        String url = String.format("/v1/round/createRound/tokenTest");
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testControllerUpdateRound() throws Exception {
        String url = String.format("/v1/round/updateRoundBy/tokenTest/basedOn/WIN");
        when(roundService.findByToken("tokenTest")).thenReturn(round);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentRound").value(1));
    }

    @Test
    public void testControllerFindHighScore() throws Exception {
        String url = String.format("/v1/round/findHighScore");
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

}
