package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service.PlayerLevelServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PlayerLevelController.class)
public class PlayerLevelControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PlayerLevelServiceImpl playerLevelService;

    private PlayerLevel playerLevel;

    @BeforeEach
    public void setUp(){
        playerLevel = new PlayerLevel();
        playerLevelService.save("tokenTest", playerLevel);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerFindByTokenLevel() throws Exception{
        when(playerLevelService.findByToken("tokenTest")).thenReturn(playerLevel);
        mvc.perform(get("/v1/level/findLevelBy/tokenTest").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentLevel").value(1));
        when(playerLevelService.findByToken("tokenTest")).thenReturn(null);
        mvc.perform(get("/v1/level/findLevelBy/tokenTestBelumAda").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerCreateLevel() throws Exception {
        String url = String.format("/v1/level/createLevel/tokenTest");
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testControllerUpdateLevel() throws Exception {
        String url = String.format("/v1/level/updateLevelAndEXPBy/tokenTest/basedOn/WIN");
        when(playerLevelService.findByToken("tokenTest")).thenReturn(playerLevel);
        mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentLevel").value(1));
    }
}
