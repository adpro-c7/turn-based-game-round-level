package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerLevelTest {

    private Class<?> playerLevelClass;

    @InjectMocks
    private PlayerLevel playerLevel;

    @BeforeEach
    public void setUp() throws Exception {
        playerLevelClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round");
        playerLevel = new PlayerLevel();
        playerLevel.addExperience();
        playerLevel.setCurrentLevel();
    }

    @Test
    public void testPlayerLevelIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(playerLevelClass.getModifiers()));
    }

    @Test
    public void testAddExperienceMethodReturnCorrectAnswer() throws Exception{
        playerLevel.addExperience();
        assertEquals(400, playerLevel.getExperience());
    }

    @Test
    public void testResetLevelMethodReturnCorrectAnswer() throws Exception{
        playerLevel.resetLevel();
        assertEquals(1, playerLevel.getCurrentLevel());
    }

    @Test
    public void testGetCurrentLevelReturnCorrectAnswer() throws Exception{
        assertEquals(1, playerLevel.getCurrentLevel());
    }

    @Test
    public void testGetExperienceReturnCorrectAnswer() throws Exception{
        playerLevel.addExperience();
        playerLevel.setCurrentLevel();
        assertEquals(400, playerLevel.getExperience());
    }

    @Test
    public void testSetCurrentLevelMethodReturnCorrectAnswer() throws Exception{
        for (int i=0; i<5; i++){
            playerLevel.addExperience();
        }
        playerLevel.setCurrentLevel();
        assertEquals(2, playerLevel.getCurrentLevel());

        playerLevel.resetLevel();
        playerLevel.addExperience();
        playerLevel.setCurrentLevel();
        assertEquals(200, playerLevel.getExperience());
    }

    @Test
    public void testUpdateReturnCorrectAnswer() throws Exception{
        playerLevel.update("WIN");
        assertEquals(400, playerLevel.getExperience());
        assertEquals(1, playerLevel.getCurrentLevel());

        playerLevel.update("LOSE");
        assertEquals(0, playerLevel.getExperience());
        assertEquals(1, playerLevel.getCurrentLevel());

    }

}

