package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class PlayerLevelRepositoryTest {

    private PlayerLevelRepository playerLevelRepository;

    @Mock
    private Map<String, PlayerLevel> playerLevelMap;

    private PlayerLevel playerLevel;

    @BeforeEach
    public void setUp() {
        playerLevelRepository = new PlayerLevelRepository();
        playerLevelMap = new HashMap<>();
        playerLevel = new PlayerLevel();
        playerLevelMap.put("token", playerLevel);
    }

    @Test
    public void whenPlayerLevelRepositoryFindAllItShouldReturnLevelMapList() {
        ReflectionTestUtils.setField(playerLevelRepository, "playerLevelMap", playerLevelMap);

        List<PlayerLevel> acquiredLevel = playerLevelRepository.findAll();

        assertThat(acquiredLevel).isEqualTo(new ArrayList<>(playerLevelMap.values()));
    }

    @Test
    public void whenPlayerLevelRepositoryFindByTokenItShouldReturnPlayerLevel() {
        ReflectionTestUtils.setField(playerLevelRepository, "playerLevelMap", playerLevelMap);
        PlayerLevel acquiredLevel = playerLevelRepository.findByToken("token");

        assertThat(acquiredLevel).isEqualTo(playerLevel);
    }

    @Test
    public void whenPlayerLevelRepositorySaveItShouldSaveLevel() {
        ReflectionTestUtils.setField(playerLevelRepository, "playerLevelMap", playerLevelMap);
        PlayerLevel newPlayerLevel = new PlayerLevel();
        playerLevelRepository.save("token2", newPlayerLevel);
        PlayerLevel acquiredNewPlayerLevel = playerLevelRepository.findByToken("token2");

        assertThat(acquiredNewPlayerLevel).isEqualTo(newPlayerLevel);
    }

    @Test
    public void whenPlayerLevelRepositoryDeleteByTokenItShouldReturnLevel() {
        ReflectionTestUtils.setField(playerLevelRepository, "playerLevelMap", playerLevelMap);
        playerLevelRepository.deleteByToken("token");
        PlayerLevel acquiredLevel = playerLevelRepository.findByToken("token");

        assertThat(acquiredLevel).isEqualTo(null);
    }
}