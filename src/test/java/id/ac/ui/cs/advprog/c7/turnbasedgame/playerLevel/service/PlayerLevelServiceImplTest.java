package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.repository.PlayerLevelRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service.PlayerLevelService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service.PlayerLevelServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PlayerLevelServiceImplTest {

    private Class<?> roundServiceClass;

    @Mock
    PlayerLevelRepository playerLevelRepository;

    PlayerLevelService playerLevelService;

    @BeforeEach
    public void setup() throws Exception {
        roundServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service.PlayerLevelServiceImpl");
        playerLevelRepository = new PlayerLevelRepository();
        playerLevelService = new PlayerLevelServiceImpl();
        PlayerLevel levelTest = new PlayerLevel();
        playerLevelRepository.save("tokenTest", levelTest);
    }

    @Test
    public void testPlayerLevelServiceHasFindAllMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        List<PlayerLevel> acquiredLevel = playerLevelService.findAll();

        assertThat(acquiredLevel).isEqualTo(playerLevelRepository.findAll());
    }

    @Test
    public void testPlayerLevelServiceHasFindByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        PlayerLevel acquiredLevel = playerLevelService.findByToken("tokenTest");

        assertThat(acquiredLevel).isEqualTo(playerLevelRepository.findByToken("tokenTest"));
    }

    @Test
    public void testPlayerLevelServiceHasSaveMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        PlayerLevel playerLevel = new PlayerLevel();
        playerLevelService.save("token", playerLevel);
        PlayerLevel acquiredLevel = playerLevelService.findByToken("token");

        assertThat(acquiredLevel).isEqualTo(playerLevelRepository.findByToken("token"));
    }

    @Test
    public void testPlayerLevelServiceHasDeleteByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        PlayerLevel playerLevel = new PlayerLevel();
        playerLevelService.save("token", playerLevel);
        playerLevelService.deleteByToken("token");
        PlayerLevel acquiredLevel = playerLevelService.findByToken("token");

        assertThat(acquiredLevel).isEqualTo(null);
    }

    @Test
    public void testPlayerLevelServiceHasUpdateMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        PlayerLevel playerLevel = new PlayerLevel();
        playerLevelService.save("token", playerLevel);
        playerLevelService.update("token", "WIN");
        playerLevelService.update("token", "WIN");
        playerLevelService.update("token", "WIN");
        PlayerLevel acquiredLevel = playerLevelService.findByToken("token");
        int currentLevel = acquiredLevel.getCurrentLevel();

        assertThat(currentLevel).isEqualTo(playerLevelRepository.findByToken("token").getCurrentLevel());
        assertThat(currentLevel).isEqualTo(2);
    }

    @Test
    public void testPlayerLevelServiceHasCreateLevelMethod() throws Exception {
        ReflectionTestUtils.setField(playerLevelService, "playerLevelRepository", playerLevelRepository);
        playerLevelService.createLevel("token");
        PlayerLevel playerLevel = playerLevelService.findByToken("token");
        assertThat(playerLevel).isEqualTo(playerLevelRepository.findByToken("token"));
    }
}