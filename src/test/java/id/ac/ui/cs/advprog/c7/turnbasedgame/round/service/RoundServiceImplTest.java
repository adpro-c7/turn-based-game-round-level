package id.ac.ui.cs.advprog.c7.turnbasedgame.round.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.repository.RoundRepository;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RoundServiceImplTest {

    private Class<?> roundServiceClass;

    @Mock
    RoundRepository roundRepository;

    RoundService roundService;

    @BeforeEach
    public void setup() throws Exception {
        roundServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundServiceImpl");
        roundRepository = new RoundRepository();
        roundService = new RoundServiceImpl();
        Round roundTest = new Round();
        roundRepository.save("tokenTest", roundTest);
    }

    @Test
    public void testRoundServiceHasFindAllMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        List<Round> acquiredRound = roundService.findAll();

        assertThat(acquiredRound).isEqualTo(roundRepository.findAll());
    }

    @Test
    public void testRoundServiceHasFindByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        Round acquiredRound = roundService.findByToken("tokenTest");

        assertThat(acquiredRound).isEqualTo(roundRepository.findByToken("tokenTest"));
    }

    @Test
    public void testRoundServiceReturnCorrectHighScore() throws Exception{
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);

        Round round1 = new Round();
        for (int i=1; i<=5; i++){
            round1.addRound();
        }
        round1.setNewMaxRound();
        roundRepository.save("token1", round1);

        Round round2 = new Round();
        for (int i=1; i<=3; i++){
            round2.addRound();
        }
        round2.setNewMaxRound();
        roundRepository.save("token2", round2);

        Round round3 = new Round();
        for (int i=1; i<=10; i++){
            round3.addRound();
        }
        round3.setNewMaxRound();
        roundRepository.save("token3", round3);

        int highScore = roundService.findHighScore();
        assertEquals(10, highScore);

    }

    @Test
    public void testRoundServiceHasSaveMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        Round round4 = new Round();
        roundService.save("token4", round4);
        Round acquiredRound = roundService.findByToken("token4");

        assertThat(acquiredRound).isEqualTo(roundRepository.findByToken("token4"));
    }

    @Test
    public void testRoundServiceHasDeleteByTokenMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        Round round4 = new Round();
        roundService.save("token4", round4);
        roundService.deleteByToken("token4");
        Round acquiredRound = roundService.findByToken("token4");

        assertThat(acquiredRound).isEqualTo(null);
    }

    @Test
    public void testRoundServiceHasUpdateMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        Round round4 = new Round();
        roundService.save("token4", round4);
        roundService.update("token4", "WIN");
        roundService.update("token4", "WIN");
        Round acquiredRound = roundService.findByToken("token4");
        int currentRound = acquiredRound.getCurrentRound();

        assertThat(currentRound).isEqualTo(roundRepository.findByToken("token4").getCurrentRound());
        assertThat(currentRound).isEqualTo(3);
    }

    @Test
    public void testRoundServiceHasCreateRoundMethod() throws Exception {
        ReflectionTestUtils.setField(roundService, "roundRepository", roundRepository);
        roundService.createRound("token4");
        Round round = roundService.findByToken("token4");
        assertThat(round).isEqualTo(roundRepository.findByToken("token4"));
    }

}
