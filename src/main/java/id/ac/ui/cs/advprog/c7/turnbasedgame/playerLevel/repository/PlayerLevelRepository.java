package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerLevelRepository {

    private Map<String, PlayerLevel> playerLevelMap = new HashMap<>();

    public PlayerLevelRepository(){}

    public List<PlayerLevel> findAll() {
        return new ArrayList(playerLevelMap.values());
    }

    public void save(String token, PlayerLevel playerLevel) {
        playerLevelMap.put(token, playerLevel);
    }

    public PlayerLevel findByToken(String token) {
        return playerLevelMap.get(token);
    }

    public void deleteByToken(String token){
        playerLevelMap.remove(token);
    }
}
