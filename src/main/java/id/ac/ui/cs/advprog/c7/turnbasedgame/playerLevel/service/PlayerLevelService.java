package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import java.util.List;

public interface PlayerLevelService {
    public void createLevel(String token);
    public PlayerLevel findByToken(String token);
    public List<PlayerLevel> findAll();
    public void update(String token, String state);
    public void save(String token, PlayerLevel round);
    public void deleteByToken(String token);
}
