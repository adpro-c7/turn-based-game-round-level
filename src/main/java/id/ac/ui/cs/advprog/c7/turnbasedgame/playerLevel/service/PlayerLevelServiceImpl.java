package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.repository.PlayerLevelRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerLevelServiceImpl implements PlayerLevelService{

    private PlayerLevelRepository playerLevelRepository;

    public PlayerLevelServiceImpl(){
        playerLevelRepository = new PlayerLevelRepository();
    }

    @Override
    public void createLevel(String token){
        PlayerLevel playerLevel = new PlayerLevel();
        save(token, playerLevel);
    }

    @Override
    public PlayerLevel findByToken(String token){
        return playerLevelRepository.findByToken(token);
    }

    @Override
    public List<PlayerLevel> findAll(){
        return playerLevelRepository.findAll();
    }

    @Override
    public void update(String token, String state){
        PlayerLevel playerLevel = findByToken(token);
        playerLevel.update(state);
        save(token, playerLevel);
    }

    @Override
    public void save(String token, PlayerLevel playerLevel){
        playerLevelRepository.save(token, playerLevel);
    }

    @Override
    public void deleteByToken(String token){
        playerLevelRepository.deleteByToken(token);
    }
}
