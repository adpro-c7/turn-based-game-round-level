package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

public class Round{

    private int currentRound;
    private int maxRound;

    public Round(){
        this.currentRound = 1;
        this.maxRound = 0;
    }

    public void addRound(){
        this.currentRound = this.currentRound + 1;
    }

    public void resetRound(){
        this.currentRound = 0;
    }

    public void setNewMaxRound(){
        if (getCurrentRound() - 1 > getMaxRound()){
            this.maxRound = getCurrentRound() - 1;
        }
    }

    public void update(String state){
        if (state.equals("WIN")){
            addRound();
            setNewMaxRound();
        } else if (state.equals("LOSE")){
            setNewMaxRound();
            resetRound();
        }
    }

    public int getCurrentRound(){
        return this.currentRound;
    }

    public int getMaxRound(){
        return this.maxRound;
    }

}
