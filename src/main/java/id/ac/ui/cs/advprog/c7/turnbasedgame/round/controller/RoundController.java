package id.ac.ui.cs.advprog.c7.turnbasedgame.round.controller;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
@RequestMapping(path = "/v1/round")
public class RoundController {
    @Autowired
    private RoundService roundService;

    @GetMapping(path = "/createRound/{token}", produces = {"application/json"})
    @ResponseBody
    public void createRound(@PathVariable(value = "token") String token) {
        roundService.createRound(token);
    }

    @GetMapping(path = "/findHighScore", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Integer> findHighScore() {
        int highScore = roundService.findHighScore();
        return ResponseEntity.ok((Integer)highScore);
    }

    @GetMapping(path = "/findRoundBy/{token}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity findRound(@PathVariable(value = "token") String token) {
        Round round = roundService.findByToken(token);
        if (round == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(round);
    }

    @GetMapping(path = "/updateRoundBy/{token}/basedOn/{state}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Round> updateRound(@PathVariable(value = "token") String token, @PathVariable(value = "state") String state) {
        roundService.update(token, state);
        Round round = roundService.findByToken(token);
        return ResponseEntity.ok(round);
    }

}