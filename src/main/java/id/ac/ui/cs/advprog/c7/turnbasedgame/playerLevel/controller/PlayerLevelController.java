package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.controller;

import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.service.PlayerLevelService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core.PlayerLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping(path = "/v1/level")
public class PlayerLevelController {
    @Autowired
    private PlayerLevelService playerLevelService;

    @GetMapping(path = "/createLevel/{token}", produces = {"application/json"})
    @ResponseBody
    public void createLevel(@PathVariable(value = "token") String token) {
        playerLevelService.createLevel(token);
    }

    @GetMapping(path = "/findLevelBy/{token}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity findLevel(@PathVariable(value = "token") String token) {
        PlayerLevel playerLevel = playerLevelService.findByToken(token);
        if (playerLevel == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(playerLevel);
    }

    @GetMapping(path = "/updateLevelAndEXPBy/{token}/basedOn/{state}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<PlayerLevel> updateLevel(@PathVariable(value = "token") String token, @PathVariable(value = "state") String state) {
        playerLevelService.update(token, state);
        PlayerLevel playerLevel = playerLevelService.findByToken(token);
        return ResponseEntity.ok(playerLevel);
    }
}