package id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import java.util.List;

public interface RoundService {
    public void createRound(String token);
    public int findHighScore();
    public Round findByToken(String token);
    public List<Round> findAll();
    public void update(String token, String state);
    public void save(String token, Round round);
    public void deleteByToken(String token);
}
