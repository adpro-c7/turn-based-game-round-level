package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.core;

public class PlayerLevel{

    private int currentLevel;
    private int experience;
    private int maxExperience = 500;

    public PlayerLevel(){
        this.currentLevel = 1;
        this.experience = 0;
    }

    public void addExperience(){
        this.experience = this.experience + 200;
    }

    public void resetLevel(){
        this.currentLevel = 1;
        this.experience = 0;
    }

    public void setCurrentLevel(){
        if (getExperience() > maxExperience){
            this.currentLevel = getCurrentLevel() + 1;
            this.experience = getExperience() - maxExperience;
        }
    }

    public void update(String state){
        if (state.equals("WIN")){
            addExperience();
            setCurrentLevel();
        } else if (state.equals("LOSE")){
            resetLevel();
        }
    }

    public int getCurrentLevel(){
        return this.currentLevel;
    }

    public int getExperience(){
        return this.experience;
    }
}
