package id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.repository.RoundRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RoundServiceImpl implements RoundService {

    private int highScore;

    private RoundRepository roundRepository;

    public RoundServiceImpl(){
        roundRepository = new RoundRepository();
    }

    @Override
    public void createRound(String token){
        Round round = new Round();
        save(token, round);
    }

    @Override
    public int findHighScore(){
        List<Round> listRoundOfAllGameState = findAll();

        for (Round roundOfAGameState : listRoundOfAllGameState){
            int maxRoundOfAGameState = roundOfAGameState.getMaxRound();
            if (maxRoundOfAGameState > this.highScore){
                this.highScore = maxRoundOfAGameState;
            }
        }

        return this.highScore;
    }

    @Override
    public Round findByToken(String token){
        return roundRepository.findByToken(token);
    }

    @Override
    public List<Round> findAll(){
        return roundRepository.findAll();
    }

    @Override
    public void update(String token, String state){
        Round round = findByToken(token);
        round.update(state);
        save(token, round);
    }

    @Override
    public void save(String token, Round round){
        roundRepository.save(token, round);
    }

    @Override
    public void deleteByToken(String token){
        roundRepository.deleteByToken(token);
    }
}
