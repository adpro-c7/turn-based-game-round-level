package id.ac.ui.cs.advprog.c7.turnbasedgame.round.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RoundRepository {

    private Map<String, Round> roundMap = new HashMap<>();

    public RoundRepository(){}

    public List<Round> findAll() {
        return new ArrayList(roundMap.values());
    }

    public void save(String token, Round round) {
        roundMap.put(token, round);
    }

    public Round findByToken(String token) {
        return roundMap.get(token);
    }

    public void deleteByToken(String token){
        roundMap.remove(token);
    }

}
