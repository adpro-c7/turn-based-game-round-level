# Group Project C7
## Advance Programming 2020/2021
[![pipeline status](https://gitlab.com/adpro-c7/turn-based-game-round-level/badges/master/pipeline.svg)](https://gitlab.com/adpro-c7/turn-based-game-round-level/-/commits/master/)
[![coverage report](https://gitlab.com/adpro-c7/turn-based-game-round-level/badges/master/coverage.svg)](https://gitlab.com/adpro-c7/turn-based-game-round-level/-/commits/master/)

# Turn-based Game
Web game where we can fight enemies.</br>
Service : Round dan Player Level

### Repository:
[https://gitlab.com/adpro-c7/turn-based-game-round-level](https://gitlab.com/adpro-c7/turn-based-game-round-level)

### Deployed Site:
[http://c7-game-round-playerlevel.herokuapp.com/](http://c7-game-round-playerlevel.herokuapp.com/)

### Patterns Implemented:
- Observer Pattern